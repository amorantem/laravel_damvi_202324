<?php

namespace App\Http\Controllers;

use App\Models\Menjar;
use App\Models\Recepta;
use Illuminate\Http\Request;

class MenjarController extends Controller
{
    public function newMenjar()
    {
        $menjar = new Menjar();
        $menjar->nom = 'Poma';
        $menjar->quantitat = 100;
        $menjar->gust = 'dolç';
        $menjar->sa = true;
        $menjar->preu = 1.5;
        $menjar->calories = 52.1;

        //Amb el metode save() guardem el nou registre
        $menjar->save();

        return "Poma generada!!!";
    }

    public function findPoma()
    {
        //Amb el metode where podem filtrar com fariem a un where d'SQL normal. El metode first ens retorna el primer element que compleix les condicions
        //El metode get() ens retorna una col·leccio amb tots els elements que compleixin les condicions.
        $poma = Menjar::where('nom', 'poma')->first();
        return "He trobar una poma. El seu preu és de: ".$poma->preu." Encara em queden ".$poma->quantitat." unitats";
    }

    public function getAllMenjar()
    {
        //Amb el metode all() obtenim tots els registres de la taula indicada
        $menjars = Menjar::all();
        return $menjars;
    }

    public function transformFruit($id,$newNom)
    {
        $menjar = Menjar::find($id);
        $lastName = $menjar->nom;
        $menjar->nom = $newNom;
        //Per fer updates, només hem de cridar al mètode save del mateix objecte que estem modificant
        $menjar->save();

        return "El menjar: ".$lastName." ara es diu: ".$newNom;
    }

    public function getMenjar()
    {
        $recepta = Recepta::find(1);
        return $recepta->menjar;
    }

    public function byebyeFruit($id)
    {
        $menjar = Menjar::find($id);
        //Per eliminar registres fem servir el metode delete()
        return $menjar->delete();
    }

}
