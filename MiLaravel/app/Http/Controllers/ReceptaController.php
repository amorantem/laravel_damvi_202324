<?php

namespace App\Http\Controllers;

use App\Models\Menjar;
use App\Models\Recepta;
use Illuminate\Http\Request;

class ReceptaController extends Controller
{
    public function newRecepta($idMenjar)
    {
        $newRecepta = new Recepta();
        $newRecepta->temps_elaboracio = 5;
        $newRecepta->nivell = 1;
        $newRecepta->save();


        $menjar = Menjar::find($idMenjar);
        $menjar->recepta_id = $newRecepta->id;
        $menjar->save();
    }

    public function getRecepta()
    {
        $menjar = Menjar::find(1);
        return $menjar->recepta;
    }
}
