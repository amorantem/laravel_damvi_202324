<?php

namespace App\Http\Controllers;

use App\Models\Cobert;
use App\Models\Cuiner;
use App\Models\Menjar;
use App\Models\Recepta;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    public function suma($valor1=null, $valor2=null)
    {
        if($valor1===null || $valor2===null)
            return "qué vols sumar??????????";
        return $valor1+$valor2;
    }

    public function newCuiner()
    {
        $cuiner = new Cuiner();
        $cuiner->nom = "Remy";
        $cuiner->edat = 1000;
        $cuiner->experiencia = false;
        //Fins que no fem el save, la id no es genera.
        $cuiner->save();

        $recepta = Recepta::find(1);
        $recepta->cuiner_id = $cuiner->id;
        $recepta->save();
    }

    public function getReceptes()
    {
        $cuiner = Cuiner::find(5);
        return $cuiner->recepta_cuinada;
    }

    public function saveCobert(Request $r)
    {
        $cobert = new Cobert();
        $cobert->nom = $r->nom;
        $cobert->material = $r->material;
        $cobert->save();
        $cobert->menjars()->attach($r->menjar);

    }

    public function getAllCoberts($id)
    {
        $menjar = Menjar::find($id);
        return $menjar->coberts;
    }
}
