<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menjar extends Model
{
    use HasFactory;
    protected $fillable = ['nom', 'calories', 'gust', 'quantitat', 'preu', 'sa'];
    public function recepta()
    {
        //Amb aquest metode indiquem una relacio One To One. A l'altra entitat relacionada, li ficarem el metode "hasOne()"
        return $this->belongsTo(Recepta::class);
    }

    public function coberts()
    {
        return $this->belongsToMany(Cobert::class,'menjars_coberts', 'menjar_id', 'cobert_id');
    }
}
