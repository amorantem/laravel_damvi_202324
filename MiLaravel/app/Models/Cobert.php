<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cobert extends Model
{
    use HasFactory;
    protected $fillable = ['material', 'nom'];

    public function menjars()
    {
        //Aquest metode marca una relacion many to many. $this->belongsToMany([classe del model amb el que es relaciona]
        //, [nom de la taula intermitja]
        //, [nom de la columna a on es troba la id que representa al model actual en la taula intermitja]
        //, [nom de la columna a on es troba la id del model amb el que ens relacionem]);
        return $this->belongsToMany(Menjar::class,'menjars_coberts', 'cobert_id', 'menjar_id');
    }

}
