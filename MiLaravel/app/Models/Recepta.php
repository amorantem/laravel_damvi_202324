<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recepta extends Model
{
    use HasFactory;
    //protected $table = "receptes";
    protected $fillable = ['temps_elaboracio', 'nivell'];

    public function menjar()
    {
        //Amb aquest metode indiquem una relacio One To One. A l'altra entitat relacionada, li ficarem el metode "belongsTo()"
        return $this->hasOne(Menjar::class);
    }

    public function cuiner()
    {
        //Amb aquest metode marquem una relacio 1-n. A l'altre entitat li indiquem el metode hasMany();
        return $this->belongsTo(Cuiner::class);
    }

}
