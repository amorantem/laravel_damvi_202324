<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuiner extends Model
{
    use HasFactory;
    protected $fillable = ['nom', 'direccio', 'edat', 'experiencia'];

    public function recepta_cuinada()
    {
        return $this->hasMany(Recepta::class);
    }
}
