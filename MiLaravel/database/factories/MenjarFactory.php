<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Menjar>
 */
class MenjarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nom' => Str::random(10),
            'calories' => rand(0,10000),
            'preu' => rand(1,500),
            'sa' => rand(0,1),
            'gust' => 'rpm'
        ];
    }
}
