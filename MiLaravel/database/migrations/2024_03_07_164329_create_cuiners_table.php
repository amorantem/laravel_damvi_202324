<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cuiners', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nom')->unique();
            $table->string('direccio')->nullable();
            $table->integer('edat')->default(40);
            $table->boolean('experiencia')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cuiners');
    }
};
