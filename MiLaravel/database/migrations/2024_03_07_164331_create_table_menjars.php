<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menjars', function (Blueprint $table) {
            //Amb el metode id() li indiquem que la PK serà un camp autoincremental integer.
            $table->id();
            $table->timestamps();
            $table->string('nom')->unique();
            $table->double('calories');
            $table->enum('gust',['salat', 'dolç', 'umami', 'amarg', 'rpm', 'àcid']);
            $table->integer('quantitat')->default(0);
            $table->double('preu')->nullable();
            $table->boolean('sa');
            $table->foreignId('recepta_id')->nullable()->constrained('receptas', 'id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_menjars');
    }
};
