<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('receptas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('temps_elaboracio');
            $table->integer('nivell');
            $table->foreignId('cuiner_id')->nullable()->constrained('cuiners', 'id');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('receptas');
    }
};
