<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menjars_coberts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('menjar_id')->constrained('menjars', 'id');
            $table->foreignId('cobert_id')->constrained('coberts', 'id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menjars_coberts');
    }
};
