<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/newCobert', function(){
    $a = \App\Models\Menjar::all();
    return view('newCobertForm')->with(['menjars'=>$a]);
});

Route::get('/getAllCoberts/{id}', [\App\Http\Controllers\Controller::class, 'getAllCoberts'])->name('getAllCoberts');


Route::post('new_cobert', [\App\Http\Controllers\Controller::class, 'saveCobert'])->name('saveCobert');

require __DIR__.'/auth.php';
